@extends('layouts.developer')

@section('content')

<div class="card">
    <div class="card-header bg-primary text-white"><strong>Users</strong></div>

    <div class="card-body">
        @if (session('success'))
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <strong>{{ 'Success!' }}</strong>&nbsp;&nbsp;{{ session('success') }}
            </div>
        @endif

        @if ($errors->any())
            <div class="alert alert-danger" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <strong>{{ 'Failed to execute your recent request due to the following reasons :' }}</strong>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <div class="text-right">

            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#createUserFormModal">
                <i class="fas fa-plus"></i>&nbsp;&nbsp;{{ 'Create' }}
            </button>

        </div>

        <br/>

        <div class="table-responsive">

            <table class="table table-hover">
                <thead>
                <tr>
                    <th scope="col" width="5%" class="text-center">#</th>
                    <th scope="col" width="30%">Name</th>
                    <th scope="col" width="35%">Email</th>
                    <th scope="col" width="15%">Created At</th>
                    <th scope="col" width="15%">Updated At</th>
                    <th scope="col"></th>
                </tr>
                </thead>
                <tbody>
                @foreach($data['users'] as $user)
                    <tr>
                        <th class="text-center">{{ $user->id }}</th>
                        <td>{{ $user->name }}</td>
                        <td>{{ $user->email }}</td>
                        <td>{{ $user->created_at }}</td>
                        <td>{{ $user->updated_at }}</td>
                        <td>
                            <div class="btn-group" role="group">
                                <button id="btnGroupDrop1" type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">{{ 'Action' }}&nbsp;&nbsp;<i class="fas fa-angle-dow"></i></button>
                                <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                                    <button type="button" class="dropdown-item" onclick="viewUser('{{ $user->id }}')"><i class="fas fa-eye fa-fw"></i>&nbsp;&nbsp;&nbsp;&nbsp;{{ 'Show Detail' }}</button>
                                    <button type="button" class="dropdown-item" onclick="updateUser('{{ $user->id }}')"><i class="fas fa-pencil-alt fa-fw"></i>&nbsp;&nbsp;&nbsp;&nbsp;{{ 'Update User' }}</button>
                                    <button type="button" class="dropdown-item" onclick="updatePassword('{{ $user->id }}')"><i class="fas fa-key fa-fw"></i>&nbsp;&nbsp;&nbsp;&nbsp;{{ 'Change Password' }}</button>
                                    <button type="button" class="dropdown-item" onclick="setRole('{{ $user->id }}')"><i class="fas fa-users-cog fa-fw"></i>&nbsp;&nbsp;&nbsp;&nbsp;{{ 'Set Role' }}</button>
                                    <button type="button" class="dropdown-item text-danger" onclick="confirmDelete('{{ $user->id }}')"><i class="fas fa-trash-alt fa-fw"></i>&nbsp;&nbsp;&nbsp;&nbsp;{{ 'Remove' }}</button>
                                </div>
                            </div>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>

        </div>

        <div class="float-right">

            {{ $data['users']->links() }}

        </div>

    </div>
</div>

@component('developer.user.create', ['roles' => $data['roles']])
@endcomponent

@component('developer.user.item')
@endcomponent

@component('developer.user.update')
@endcomponent

@component('developer.user.change-password')
@endcomponent

@component('developer.user.set-role', ['roles' => $data['roles']])
@endcomponent

@component('developer.user.remove')
@endcomponent

    <script>
        let usersJson = '{!! str_replace('\'', '\\\'', json_encode($data['users'])) !!}';
        let users = JSON.parse(unescape(usersJson));

       function viewUser(userId) {
           let user = getUser(userId);
           $('#itemTitle').html(user.name);
           $('#itemName').val(user.name);
           $('#itemEmail').val(user.email);
           if (user.roles.length > 0 && user.roles[0].label != null) {
               $('#itemRole').val(user.roles[0].label);
           }
           $('#itemUserFormModal').modal('show');
       }

       function updateUser(userId) {
           let user = getUser(userId);
           $('#updateId').val(user.id);
           $('#updateName').val(user.name);
           $('#updateEmail').val(user.email);
           $('#updateUserFormModal').modal('show');
       }

       function updatePassword(userId) {
           let user = getUser(userId);
           $('#changePasswordTitle').html('Change Password for ' + user.name);
           $('#changePasswordId').val(user.id);
           $('#changePasswordUserFormModal').modal('show');
       }

       function setRole(userId) {
           let user = getUser(userId);
           $('#setRoleTitle').html('Set Role for ' + user.name);
           $('#setRoleUserId').val(user.id);
           if (user.roles.length > 0 && user.roles[0].label != null) {
               $('#setRoleRoleId').val(user.roles[0].id);
           }
           $('#setRoleUserFormModal').modal('show');
       }

       function confirmDelete(userId) {
           let user = getUser(userId);
           $('#removeTitle').html('Remove ' + user.name);
           $('#removeConfirmation').html('Are you sure want to remove ' + user.name + '?');
           $('#removeId').val(user.id);
           $('#removeUserFormModal').modal('show');
       }

        function getUser(userId) {
            return users.data.filter(function (item) {
                return item.id === parseInt(userId);
            })[0];
        }
    </script>

@endsection
