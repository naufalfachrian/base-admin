<form action="{{ route('developer.user.create') }}" method="post">
@csrf
<div class="modal fade" id="createUserFormModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">{{ 'Create User' }}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label for="name">Name</label>
                    <input id="name" name="name" type="text" required="required" class="form-control here">
                </div>
                <div class="form-group">
                    <label for="email">Email</label>
                    <input id="email" name="email" type="email" required="required" class="form-control here">
                </div>
                <div class="form-group">
                    <label for="password">Password</label>
                    <input id="password" name="password" type="password" required="required" class="form-control here">
                </div>
                <div class="form-group">
                    <label for="confirm-password">Confirm Password</label>
                    <input id="confirm-password" name="password_confirmation" type="password" required="required" class="form-control here">
                </div>
                <div class="form-group">
                    <label for="role">Role</label>
                    <div>
                        <select id="role" name="role" required="required" class="custom-select">
                            @foreach($roles as $role)
                            <option value="{{ $role->id }}">{{ $role->label }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary"><i class="fas fa-plus"></i>&nbsp;&nbsp;{{ 'Create' }}</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fas fa-times"></i>&nbsp;&nbsp;{{ 'Cancel' }}</button>
            </div>
        </div>
    </div>
</div>
</form>