<form action="{{ route('developer.user.changePassword') }}" method="post">
    <label hidden for="changePasswordId"></label><input hidden id="changePasswordId" name="id">
@csrf
<div class="modal fade" id="changePasswordUserFormModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="changePasswordTitle">{{ 'Chnage Password' }}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label for="change-password-password">Password</label>
                    <input id="change-password-password" name="password" type="password" required="required" class="form-control here">
                </div>
                <div class="form-group">
                    <label for="change-password-confirm">Confirm Password</label>
                    <input id="change-password-confirm" name="password_confirmation" type="password" required="required" class="form-control here">
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary"><i class="fas fa-key"></i>&nbsp;&nbsp;{{ 'Change Password' }}</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fas fa-times"></i>&nbsp;&nbsp;{{ 'Cancel' }}</button>
            </div>
        </div>
    </div>
</div>
</form>