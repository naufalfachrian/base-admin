<form action="{{ route('developer.user.setRole') }}" method="post">
    <label for="setRoleUserId"></label><input id="setRoleUserId" hidden name="id">
@csrf
<div class="modal fade" id="setRoleUserFormModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="setRoleTitle">{{ 'Set Role' }}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label for="setRoleRoleId">Role</label>
                    <div>
                        <select id="setRoleRoleId" name="role" required="required" class="custom-select">
                            @foreach($roles as $role)
                            <option value="{{ $role->id }}">{{ $role->label }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary"><i class="fas fa-users-cog"></i>&nbsp;&nbsp;{{ 'Set Role' }}</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fas fa-times"></i>&nbsp;&nbsp;{{ 'Cancel' }}</button>
            </div>
        </div>
    </div>
</div>
</form>