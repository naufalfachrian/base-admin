<div class="modal fade" id="itemMenuFormModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="itemTitle">{{ 'Role' }}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label for="itemName">Name</label>
                    <input id="itemName" name="name" type="text" required="required" class="form-control here" disabled>
                </div>
                <div class="form-group">
                    <label for="itemRoute">Route</label>
                    <input id="itemRoute" name="route" type="text" required="required" class="form-control here" disabled>
                </div>
                <div class="form-group">
                    <label for="itemLabel">Label</label>
                    <input id="itemLabel" name="label" type="text" required="required" class="form-control here" disabled>
                </div>
                <div class="form-group">
                    <label for="itemIcon">Icon</label>
                    <div class="input-group mb-2">
                        <div class="input-group-prepend">
                            <div class="input-group-text"><i id="itemIconDisplay" class=""></i> </div>
                        </div>
                        <input id="itemIcon" name="icon" type="text" required="required" class="form-control here" disabled>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fas fa-times"></i>&nbsp;&nbsp;{{ 'Close' }}</button>
            </div>
        </div>
    </div>
</div>
