<div class="modal fade" id="itemLogFormModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="logTitle">{{ '' }}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <span class="badge badge-primary" id="logMethod"></span><br/>
                <a id="logLink" target="_blank"><span id="logUrl"></span></a>
                <br/>
                <br/>
                <strong>User Agent :</strong><br/>
                <span id="logUserAgent"></span>
            </div>
            <div class="modal-footer">
                <a class="btn btn-primary text-white" target="_blank" id="showRequestButton">{{ 'Show Request' }}</a>
                <a class="btn btn-primary text-white" target="_blank" id="showResponseButton">{{ 'Show Response' }}</a>
                <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fas fa-times"></i>&nbsp;&nbsp;{{ 'Close' }}</button>
            </div>
        </div>
    </div>
</div>
