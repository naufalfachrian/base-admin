@extends('layouts.developer')

@section('content')

<div class="card">
    <div class="card-header bg-primary text-white"><strong>Logs</strong></div>

    <div class="card-body">
        @if (session('success'))
            <div class="alert alert-success" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <strong>{{ 'Success!' }}</strong>&nbsp;&nbsp;{{ session('success') }}
            </div>
        @endif

        @if ($errors->any())
            <div class="alert alert-danger" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <strong>{{ 'Failed to execute your recent request due to the following reasons :' }}</strong>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <br/>

        <div class="table-responsive">

            <table class="table table-hover">
                <thead>
                <tr>
                    <th scope="col" width="5%" class="text-center">#</th>
                    <th scope="col" width="10%">IP Address</th>
                    <th scope="col" width="5%">Method</th>
                    <th scope="col" width="10%">User</th>
                    <th scope="col" width="35%">Description</th>
                    <th scope="col" width="10%">Request Size</th>
                    <th scope="col" width="10%">Response Size</th>
                    <th scope="col" width="15%">Created At</th>
                    <th scope="col"></th>
                </tr>
                </thead>
                <tbody>
                @foreach($data['logs'] as $log)
                    <tr class="@if (strpos($log->simple_description, 'failed') !== false) table-warning @elseif (strpos($log->simple_description, 'Fatal : Exception') !== false) table-danger @elseif (strpos($log->simple_description, 'created') !== false) table-success @elseif (strpos($log->simple_description, 'removed') !== false) table-secondary @elseif ($log->method == 'POST') table-info @endif">
                        <th class="text-center">{{ $log->id }}</th>
                        <td>{{ $log->ip_address }}</td>
                        <td><span class="badge @if ($log->method == 'GET') badge-primary @else badge-danger @endif">{{ $log->method }}</span></td>
                        <td>@if ($log->user != null) {{ $log->user->name }} @else {{ '-' }} @endif</td>
                        <td>{{ $log->simple_description }}</td>
                        <td>{{ number_format(strlen($log->request), 0, ',', '.') }}</td>
                        <td>{{ number_format(strlen($log->response), 0, ',', '.') }}</td>
                        <td>{{ $log->created_at }}</td>
                        <td>
                            <div class="btn-group" role="group">
                                <button id="btnGroupDrop1" type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">{{ 'Action' }}&nbsp;&nbsp;<i class="fas fa-angle-dow"></i></button>
                                <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                                    <button type="button" class="dropdown-item" onclick="viewLog('{{ $log->id }}')"><i class="fas fa-eye fa-fw"></i>&nbsp;&nbsp;&nbsp;&nbsp;{{ 'Show Detail' }}</button>
                                </div>
                            </div>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>

        </div>

        <div class="float-right">

            {{ $data['logs']->links() }}

        </div>

    </div>
</div>

@component('developer.log.item')
@endcomponent

<script>
    let logsJson = '{!! str_replace('\'', '\\\'', json_encode($data['logs'])) !!}';
    let logs = JSON.parse(unescape(logsJson));

    function viewLog(logId) {
        let log = getLog(logId);
        $('#itemLogFormModal').modal('show');
        $('#logTitle').html(log.simple_description);
        $('#logMethod').html(log.method);
        $('#logUrl').html(log.url);
        $('#logUserAgent').html(log.user_agent);
        $('#showRequestButton').attr('href', '{{ route('developer.log.index') }}?request=' + log.id);
        $('#showResponseButton').attr('href', '{{ route('developer.log.index') }}?response=' + log.id);
        $('#logLink').attr('href', log.url);
    }

    function getLog(roleId) {
        return logs.data.filter(function (item) {
            return item.id === parseInt(roleId);
        })[0];
    }
</script>

@endsection
