@extends('layouts.developer')

@section('content')

<div class="card">
    <div class="card-header bg-primary text-white"><b>Home</b></div>

    <div class="card-body">
        @if (session('status'))
            <div class="alert alert-success" role="alert">
                {{ session('status') }}
            </div>
        @endif

        Developer Landing Page!
    </div>
</div>

@endsection
