@extends('layouts.developer')

@section('content')

<div class="card">
    <div class="card-header bg-primary text-white"><strong>Roles</strong></div>

    <div class="card-body">
        @if (session('success'))
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <strong>{{ 'Success!' }}</strong>&nbsp;&nbsp;{{ session('success') }}
            </div>
        @endif

        @if ($errors->any())
            <div class="alert alert-danger" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <strong>{{ 'Failed to execute your recent request due to the following reasons :' }}</strong>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <div class="text-right">

            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#createRoleFormModal">
                <i class="fas fa-plus"></i>&nbsp;&nbsp;{{ 'Create' }}
            </button>

        </div>

        <br/>

        <div class="table-responsive">

            <table class="table table-hover">
                <thead>
                <tr>
                    <th scope="col" width="5%" class="text-center">#</th>
                    <th scope="col" width="15%">Label</th>
                    <th scope="col" width="15%">Name</th>
                    <th scope="col" width="35%">Description</th>
                    <th scope="col" width="15%">Created At</th>
                    <th scope="col" width="15%">Updated At</th>
                    <th scope="col"></th>
                </tr>
                </thead>
                <tbody>
                @foreach($data['roles'] as $role)
                    <tr>
                        <th class="text-center">{{ $role->id }}</th>
                        <td>{{ $role->label }}</td>
                        <td>{{ $role->name }}</td>
                        <td>{{ $role->description }}</td>
                        <td>{{ $role->created_at }}</td>
                        <td>{{ $role->updated_at }}</td>
                        <td>
                            <div class="btn-group" role="group">
                                <button id="btnGroupDrop1" type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">{{ 'Action' }}&nbsp;&nbsp;<i class="fas fa-angle-dow"></i></button>
                                <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                                    <button type="button" class="dropdown-item" onclick="viewRole('{{ $role->id }}')"><i class="fas fa-eye fa-fw"></i>&nbsp;&nbsp;&nbsp;&nbsp;{{ 'Show Detail' }}</button>
                                    <button type="button" class="dropdown-item" onclick="updateRole('{{ $role->id }}')"><i class="fas fa-pencil-alt fa-fw"></i>&nbsp;&nbsp;&nbsp;&nbsp;{{ 'Update Role' }}</button>
                                    <button type="button" class="dropdown-item text-danger" onclick="confirmDelete('{{ $role->id }}')"><i class="fas fa-trash-alt fa-fw"></i>&nbsp;&nbsp;&nbsp;&nbsp;{{ 'Remove' }}</button>
                                </div>
                            </div>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>

        </div>

        <div class="float-right">

            {{ $data['roles']->links() }}

        </div>

    </div>
</div>

@component('developer.role.create')
@endcomponent

@component('developer.role.item')
@endcomponent

@component('developer.role.update')
@endcomponent

@component('developer.role.remove')
@endcomponent

<script>
    let rolesJson = '{!! str_replace('\'', '\\\'', json_encode($data['roles'])) !!}';
    let roles = JSON.parse(unescape(rolesJson));

    function viewRole(roleId) {
        let role = getRole(roleId);
        $('#itemRoleFormTitle').html(role.label);
        $('#itemName').val(role.name);
        $('#itemLabel').val(role.label);
        $('#itemDescription').val(role.description);
        $('#itemRoleFormModal').modal('show');
    }

    function updateRole(roleId) {
        let role = getRole(roleId);
        $('#updateId').val(role.id);
        $('#updateName').val(role.name);
        $('#updateLabel').val(role.label);
        $('#updateDescription').val(role.description);
        $('#updateRoleFormModal').modal('show');
    }

    function confirmDelete(roleId) {
        let role = getRole(roleId);
        $('#removeTitle').html('Remove ' + role.label);
        $('#removeConfirmation').html('Are you sure want to remove ' + role.label + '?<br/><br/><b>' + role.users.length + ' user(s) will be affected.</b>');
        $('#removeId').val(role.id);
        $('#removeRoleFormModal').modal('show');
    }

    function getRole(roleId) {
        return roles.data.filter(function (item) {
            return item.id === parseInt(roleId);
        })[0];
    }
</script>

@endsection
