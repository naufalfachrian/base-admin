@extends('layouts.developer')

@section('content')

<div class="card">
    <div class="card-header bg-primary text-white"><strong>Menus</strong></div>

    <div class="card-body">
        @if (session('success'))
            <div class="alert alert-success" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <strong>{{ 'Success!' }}</strong>&nbsp;&nbsp;{{ session('success') }}
            </div>
        @endif

        @if ($errors->any())
            <div class="alert alert-danger" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <strong>{{ 'Failed to execute your recent request due to the following reasons :' }}</strong>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <div class="text-right">

            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#createMenuFormModal">
                <i class="fas fa-plus"></i>&nbsp;&nbsp;{{ 'Create' }}
            </button>

        </div>

        <br/>

        <div class="table-responsive">

            <table class="table table-hover">
                <thead>
                <tr>
                    <th scope="col" width="5%" class="text-center">#</th>
                    <th scope="col" width="15%">Name</th>
                    <th scope="col" width="15%">Route</th>
                    <th scope="col" width="15%">Label</th>
                    <th scope="col" width="15%">Icon</th>
                    <th scope="col" width="15%">Created At</th>
                    <th scope="col" width="15%">Updated At</th>
                    <th scope="col"></th>
                </tr>
                </thead>
                <tbody>
                @foreach($data['menus'] as $menu)
                    <tr>
                        <th class="text-center">{{ $menu->id }}</th>
                        <td>{{ $menu->name }}</td>
                        <td>{{ $menu->route }}</td>
                        <td>{{ $menu->label }}</td>
                        <td><i class="{{ $menu->icon }}"></i>&nbsp;&nbsp;&nbsp;{{ $menu->icon }}</td>
                        <td>{{ $menu->created_at }}</td>
                        <td>{{ $menu->updated_at }}</td>
                        <td>
                            <div class="btn-group" role="group">
                                <button id="btnGroupDrop1" type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">{{ 'Action' }}&nbsp;&nbsp;<i class="fas fa-angle-dow"></i></button>
                                <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                                    <button type="button" class="dropdown-item" onclick="viewMenu('{{ $menu->id }}')"><i class="fas fa-eye fa-fw"></i>&nbsp;&nbsp;&nbsp;&nbsp;{{ 'Show Detail' }}</button>
                                    <button type="button" class="dropdown-item" onclick="updateMenu('{{ $menu->id }}')"><i class="fas fa-pencil-alt fa-fw"></i>&nbsp;&nbsp;&nbsp;&nbsp;{{ 'Update Menu' }}</button>
                                    <button type="button" class="dropdown-item text-danger" onclick="confirmDelete('{{ $menu->id }}')"><i class="fas fa-trash-alt fa-fw"></i>&nbsp;&nbsp;&nbsp;&nbsp;{{ 'Remove' }}</button>
                                </div>
                            </div>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>

        </div>

        <div class="float-right">

            {{ $data['menus']->links() }}

        </div>

    </div>
</div>

@component('developer.menu.create')
@endcomponent

@component('developer.menu.item')
@endcomponent

@component('developer.menu.update')
@endcomponent

@component('developer.menu.remove')
@endcomponent

<script>
    let menusJson = '{!! str_replace('\'', '\\\'', json_encode($data['menus'])) !!}';
    let menus = JSON.parse(unescape(menusJson));

    function viewMenu(menuId) {
        let menu = getMenu(menuId);
        $('#itemTitle').html(menu.label);
        $('#itemName').val(menu.name);
        $('#itemLabel').val(menu.label);
        $('#itemRoute').val(menu.route);
        $('#itemIcon').val(menu.icon);
        $('#itemIconDisplay').removeAttr('class').attr('class', menu.icon);
        $('#itemMenuFormModal').modal('show');
    }

    function updateMenu(menuId) {
        let menu = getMenu(menuId);
        $('#updateId').val(menuId);
        $('#updateName').val(menu.name);
        $('#updateLabel').val(menu.label);
        $('#updateRoute').val(menu.route);
        $('#updateIcon').val(menu.icon);
        $('#updateMenuFormModal').modal('show');
    }

    function confirmDelete(menuId) {
        let menu = getMenu(menuId);
        $('#removeTitle').html('Remove ' + menu.label);
        $('#removeConfirmation').html('Are you sure want to remove ' + menu.label + '?<br/><br/><b>' + menu.roles.length + ' role(s) will be affected.</b>');
        $('#removeId').val(menu.id);
        $('#removeMenuFormModal').modal('show');
    }

    function getMenu(menuId) {
        return menus.data.filter(function (item) {
            return item.id === parseInt(menuId);
        })[0];
    }
</script>

@endsection
