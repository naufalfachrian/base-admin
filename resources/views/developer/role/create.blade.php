<form action="{{ route('developer.role.create') }}" method="post">
@csrf
<div class="modal fade" id="createRoleFormModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">{{ 'Create Role' }}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label for="label">Label</label>
                    <input id="label" name="label" type="text" required="required" class="form-control here">
                </div>
                <div class="form-group">
                    <label for="name">Name</label>
                    <input id="name" name="name" type="text" required="required" class="form-control here">
                </div>
                <div class="form-group">
                    <label for="description">Description</label>
                    <input id="description" name="description" type="text" required="required" class="form-control here">
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary"><i class="fas fa-plus"></i>&nbsp;&nbsp;{{ 'Create' }}</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fas fa-times"></i>&nbsp;&nbsp;{{ 'Cancel' }}</button>
            </div>
        </div>
    </div>
</div>
</form>