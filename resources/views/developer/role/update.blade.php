<form action="{{ route('developer.role.update') }}" method="post">
    <label hidden for="updateId"></label><input hidden value="" name="id" id="updateId">
    @csrf
<div class="modal fade" id="updateRoleFormModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="updateRoleFormTitle">{{ 'Update Role' }}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label for="updateLabel">Label</label>
                    <input id="updateLabel" name="label" type="text" required="required" class="form-control here">
                </div>
                <div class="form-group">
                    <label for="updateName">Name</label>
                    <input id="updateName" name="name" type="text" required="required" class="form-control here">
                </div>
                <div class="form-group">
                    <label for="updateDescription">Description</label>
                    <input id="updateDescription" name="description" type="text" required="required" class="form-control here">
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary"><i class="fas fa-pencil-alt"></i>&nbsp;&nbsp;{{ 'Save' }}</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fas fa-times"></i>&nbsp;&nbsp;{{ 'Close' }}</button>
            </div>
        </div>
    </div>
</div>
</form>