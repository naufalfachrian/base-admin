<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('login');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::prefix('developer')->group(function () {

    Route::prefix('users')->group(function () {

        Route::get('/', 'UserController@index')->name('developer.user.index');

        Route::post('/', 'UserController@create')->name('developer.user.create');

        Route::post('/update', 'UserController@update')->name('developer.user.update');

        Route::post('/change-password', 'UserController@changePassword')->name('developer.user.changePassword');

        Route::post('/set-role', 'UserController@setRole')->name('developer.user.setRole');

        Route::post('/remove', 'UserController@remove')->name('developer.user.remove');

    });

    Route::prefix('roles')->group(function () {

        Route::get('/', 'RoleController@index')->name('developer.role.index');

        Route::post('/', 'RoleController@create')->name('developer.role.create');

        Route::post('/update', 'RoleController@update')->name('developer.role.update');

        Route::post('/remove', 'RoleController@remove')->name('developer.role.remove');

    });

    Route::prefix('menus')->group(function () {

        Route::get('/', 'MenuController@index')->name('developer.menu.index');

        Route::post('/', 'MenuController@create')->name('developer.menu.create');

        Route::post('/update', 'MenuController@update')->name('developer.menu.update');

        Route::post('/remove', 'MenuController@remove')->name('developer.menu.remove');

    });

    Route::prefix('/logs')->group(function () {

        Route::get('/', 'LogController@index')->name('developer.log.index');

    });

});
