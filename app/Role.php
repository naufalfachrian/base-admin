<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Role extends Model
{
    use SoftDeletes;

    protected $guarded = [];

    /**
     * Default role's name.
     *
     * @var string
     */
    protected static $defaultRole = 'default';

    /**
     * Many to many relationship between App\Role and App\User, stored on role_user table.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function users()
    {
        return $this->belongsToMany('App\User');
    }

    /**
     * Many to many relationship between App\Role and App\Menu, stored on menu_role table.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function menus()
    {
        return $this->belongsToMany('App\Menu');
    }

    /**
     * Convenience method to get the default role.
     *
     * @return mixed
     */
    public static function defaultRole()
    {
        return Role::where('name', '=', Role::$defaultRole)->firstOrFail();
    }

    /**
     * Convenience method to get the developer role.
     *
     * @return mixed
     */
    public static function developerRole()
    {
        return Role::where('name', '=', 'developer')->firstOrFail();
    }
}
