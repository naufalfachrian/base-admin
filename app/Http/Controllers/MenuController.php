<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateMenuRequest;
use App\Http\Requests\RemoveMenuRequest;
use App\Http\Requests\UpdateMenuRequest;
use App\Log;
use App\Menu;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class MenuController extends Controller
{
    /**
     * Create a new controller instance.
     */
    public function __construct()
    {
        $this->middleware(['auth']);
    }

    public function index(Request $request)
    {
        $data = [
            'user' => User::current(),
            'data' => [
                'menus' => Menu::with(['roles', 'roles.users'])->paginate(50),
            ]
        ];
        if ($request->has('debug')) {
            return $data;
        }
        Log::write(Auth::user(), 'Menu index visited', $request, $data);
        return view('developer.menu', $data);
    }

    public function create(CreateMenuRequest $request)
    {
        $menu = Menu::create($request->toArray());
        Log::write(Auth::user(), 'Menu ' . $menu->label . ' created', $request, $menu);
        return redirect()->back()->with('success', 'Menu ' . $menu->label . ' has been created.');
    }

    public function update(UpdateMenuRequest $request)
    {
        $menu = Menu::findOrFail($request->id);
        $menu->update($request->toArray());
        Log::write(Auth::user(), 'Menu ' . $menu->label . ' updated', $request, $menu);
        return redirect()->back()->with('success', 'Menu ' . $menu->label . ' has been updated.');
    }

    public function remove(RemoveMenuRequest $request)
    {
        $menu = Menu::findOrFail($request['id']);
        $menuLabel = $menu->label;
        $menu->delete();
        Log::write(Auth::user(), 'Menu ' . $menu->label . ' removed', $request, $menu);
        return redirect()->back()->with('success', $menuLabel . ' has been removed.');
    }
}
