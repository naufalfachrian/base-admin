<?php

namespace App\Http\Controllers;

use App\Log;
use App\Role;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $role = Auth::user()->roles()->firstOrFail();
        Log::write(Auth::user(), 'Home visited', $request, $role);
        if ($role->id == Role::developerRole()->id) {
            return redirect()->route('developer.log.index');
        }
        return view($role->name . '.home');
    }
}
