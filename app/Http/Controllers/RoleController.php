<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateRoleRequest;
use App\Http\Requests\RemoveRoleRequest;
use App\Http\Requests\UpdateRoleRequest;
use App\Log;
use App\Role;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class RoleController extends Controller
{
    /**
     * Create a new controller instance.
     */
    public function __construct()
    {
        $this->middleware(['auth']);
    }

    public function index(Request $request)
    {
        $data = [
            'user' => User::current(),
            'data' => [
                'roles' => Role::with(['users', 'menus'])->paginate(50),
            ]
        ];
        if ($request->has('debug')) {
            return $data;
        }
        Log::write(Auth::user(), 'Role index visited', $request, $data);
        return view('developer.role', $data);
    }

    public function create(CreateRoleRequest $request)
    {
        $role = Role::create([
            'name' => $request['name'],
            'label' => $request['label'],
            'description' => $request['description'],
        ]);
        Log::write(Auth::user(), 'Role ' . $role->label . ' created', $request, $role);
        return redirect()->back()->with('success', 'Role ' . $role->label . ' has been created.');
    }

    public function update(UpdateRoleRequest $request)
    {
        $role = Role::findOrFail($request->id);
        $role->update($request->toArray());
        Log::write(Auth::user(), 'Role ' . $role->label . ' updated', $request, $role);
        return redirect()->back()->with('success', 'Role ' . $role->label . ' has been updated.');
    }

    public function remove(RemoveRoleRequest $request)
    {
        $role = Role::findOrFail($request['id']);
        $roleLabel = $role->label;
        $role->delete();
        Log::write(Auth::user(), 'Role ' . $roleLabel . ' removed', $request, $role);
        return redirect()->back()->with('success', $roleLabel . ' has been removed.');
    }
}
