<?php

namespace App\Http\Controllers;

use App\Log;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LogController extends Controller
{
    /**
     * Create a new controller instance.
     */
    public function __construct()
    {
        $this->middleware(['auth']);
    }

    public function index(Request $request)
    {
        $data = [
            'user' => User::current(),
            'data' => [
                'logs' => Log::with(['user', 'user.roles'])->orderBy('id', 'desc')->paginate(50),
            ]
        ];
        if ($request->has('debug')) {
            return $data;
        }
        if ($request->has('request')) {
            return Log::findOrFail($request->get('request'))->request;
        }
        if ($request->has('response')) {
            return Log::findOrFail($request->get('response'))->response;
        }
        Log::write(Auth::user(), 'Log index visited', $request, []);
        return view('developer.log', $data);
    }
}
