<?php

namespace App\Http\Controllers;

use App\Http\Requests\ChangePasswordRequest;
use App\Http\Requests\CreateUserRequest;
use App\Http\Requests\RemoveUserRequest;
use App\Http\Requests\SetUserRoleRequest;
use App\Http\Requests\UpdateUserRequest;
use App\Log;
use App\Role;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     */
    public function __construct()
    {
        $this->middleware(['auth']);
    }

    public function index(Request $request)
    {
        $data = [
            'user' => User::current(),
            'data' => [
                'users' => User::with(['roles', 'roles.menus'])->paginate(50),
                'roles' => Role::all(),
            ]
        ];
        if ($request->has('debug')) {
            return $data;
        }
        Log::write(Auth::user(), 'User index visited', $request, $data);
        return view('developer.user', $data);
    }

    public function create(CreateUserRequest $request)
    {
        $user = User::create([
            'name' => $request['name'],
            'email' => $request['email'],
            'password' => Hash::make($request['password']),
        ]);
        $user->roles()->attach($request['role']);
        Log::write(Auth::user(), 'User ' . $user->name . ' created', $request, $user);
        return redirect()->back()->with('success', 'User ' . $user->name . ' has been created.');
    }

    public function update(UpdateUserRequest $request) {
        $user = User::findOrFail($request['id']);
        $user->name = $request['name'];
        $user->email = $request['email'];
        $user->save();
        Log::write(Auth::user(), 'User ' . $user->name . ' updated', $request, $user);
        return redirect()->back()->with('success', 'User ' . $user->name . ' has been updated.');
    }

    public function changePassword(ChangePasswordRequest $request) {
        $user = User::findOrFail($request['id']);
        $user->password = Hash::make($request['password']);
        $user->save();
        Log::write(Auth::user(), 'Password for ' . $user->name . ' has been changed', $request, $user);
        return redirect()->back()->with('success', 'Password for ' . $user->name . ' has been changed.');
    }

    public function setRole(SetUserRoleRequest $request) {
        $user = User::findOrFail($request['id']);
        $user->roles()->detach();
        $user->roles()->attach($request['role']);
        $role = Role::findOrFail($request['role']);
        Log::write(Auth::user(), $user->name . '\'s role has been changed to ' . $role->label, $request, $user);
        return redirect()->back()->with('success', $user->name . '\'s role has been changed to ' . $role->label . '.');
    }

    public function remove(RemoveUserRequest $request) {
        $user = User::findOrFail($request['id']);
        $userName = $user->name;
        $user->delete();
        Log::write(Auth::user(), $userName . ' has been removed', $request, $user);
        return redirect()->back()->with('success', $userName . ' has been removed.');
    }
}
