<?php

namespace App\Http\Requests;

use App\Log;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class LoggedRequest extends FormRequest
{
    protected $errorLogDescription = 'Failed';

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }

    /**
     * @inheritdoc
     */
    protected function failedValidation(Validator $validator)
    {
        Log::write(Auth::user(), $this->errorLogDescription, $this, [
            'validators' => $this->rules(),
            'errors' => $validator->errors(),
        ]);
        parent::failedValidation($validator);
    }
}
