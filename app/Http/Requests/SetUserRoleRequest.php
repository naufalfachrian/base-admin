<?php

namespace App\Http\Requests;

class SetUserRoleRequest extends LoggedRequest
{
    protected $errorLogDescription = 'Set user role failed';

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id' => ['required', 'exists:users,id'],
            'role' => ['required', 'exists:roles,id'],
        ];
    }
}
