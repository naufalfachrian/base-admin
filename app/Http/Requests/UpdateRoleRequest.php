<?php

namespace App\Http\Requests;

class UpdateRoleRequest extends LoggedRequest
{
    protected $errorLogDescription = 'Update role failed';

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id' => ['required', 'exists:roles,id'],
            'label' => ['required', 'string', 'max:255'],
            'description' => ['required', 'string', 'max:255'],
            'name' => ['required', 'string', 'max:255', 'unique:roles,name,' . $this->id . ',id'],
        ];
    }
}
