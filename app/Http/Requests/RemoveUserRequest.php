<?php

namespace App\Http\Requests;

class RemoveUserRequest extends LoggedRequest
{
    protected $errorLogDescription = 'Remove user failed';

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id' => ['required', 'exists:users,id'],
        ];
    }
}
