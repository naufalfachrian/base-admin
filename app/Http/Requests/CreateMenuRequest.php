<?php

namespace App\Http\Requests;

class CreateMenuRequest extends LoggedRequest
{
    protected $errorLogDescription = 'Create menu failed';

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'label' => ['required', 'string', 'max:255'],
            'icon' => ['required', 'string', 'max:255'],
            'route' => ['required', 'string', 'max:255'],
            'name' => ['required', 'string', 'max:255', 'unique:menus'],
        ];
    }
}
