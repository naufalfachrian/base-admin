<?php

namespace App;

use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Http\Request;

class Log extends Model
{
    use SoftDeletes;

    protected $guarded = [];

    protected $hidden = ['request', 'response'];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public static function write($user, $simpleDescription, Request $request, $response)
    {
        Log::create([
            'ip_address' => $request->ip(),
            'url' => $request->fullUrl(),
            'method' => $request->method(),
            'user_agent' => $request->header('User-Agent'),
            'user_id' => $user == null ? null : $user->id,
            'simple_description' => $simpleDescription,
            'request' => $request == null ? null : json_encode($request->all()),
            'response' => $response == null ? null : json_encode($response),
        ]);
    }
}
