<?php

use Illuminate\Database\Seeder;

class MenuSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\Menu::create([
            'name' => 'separator',
        ]);

        $homeMenu = App\Menu::create([
            'name' => 'log',
            'route' => 'developer.log.index',
            'label' => 'Logs',
            'icon' => 'fas fa-pen-nib fa-fw'
        ]);

        $userMenu = \App\Menu::create([
            'name' => 'developer.user.index',
            'route' => 'developer.user.index',
            'label' => 'Users',
            'icon' => 'fas fa-users fa-fw'
        ]);

        $roleMenu = App\Menu::create([
            'name' => 'developer.role.index',
            'route' => 'developer.role.index',
            'label' => 'Roles',
            'icon' => 'fas fa-users-cog fa-fw',
        ]);

        $menuMenu = \App\Menu::create([
            'name' => 'developer.menu.index',
            'route' => 'developer.menu.index',
            'label' => 'Menus',
            'icon' => 'fas fa-th fa-fw',
        ]);

        $menusForDeveloperRole = [
            1 => $homeMenu,
            2 => $userMenu,
            3 => $roleMenu,
            4 => $menuMenu,
        ];

        foreach ($menusForDeveloperRole as $key => $menu) {
            \App\Role::developerRole()->menus()->attach($menu->id, [
                'sequence' => $key,
            ]);
        }
    }
}
