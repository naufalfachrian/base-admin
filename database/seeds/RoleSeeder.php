<?php

use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Role::create([
            'name' => 'developer',
            'label' => 'Developer',
            'description' => 'Role for developers.'
        ]);

        \App\Role::create([
            'name' => 'default',
            'label' => 'Default Role',
            'description' => 'A default role for recently created user.'
        ]);
    }
}
