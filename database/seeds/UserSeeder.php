<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new \App\User();
        $user->name = 'Bunga Mungil';
        $user->email = 'dev@bungamungil.id';
        $user->password = Hash::make('hello-dev!');
        $user->save();
        $user->roles()->attach(\App\Role::developerRole()->id);
    }
}
