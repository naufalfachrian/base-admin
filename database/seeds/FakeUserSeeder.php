<?php

use Illuminate\Database\Seeder;

class FakeUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        for ($i = 0; $i < 2000; $i++) {
            $user = new \App\User();
            $user->name = $faker->name;
            $user->email = $faker->safeEmail;
            $user->password = Hash::make('password');
            $user->save();
            $user->roles()->attach(\App\Role::defaultRole()->id);
        }
    }
}
